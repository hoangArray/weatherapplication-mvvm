//
//  ViewModelType.swift
//  WeatherApplication
//
//  Created by Anton Hoang on 26/11/2019.
//  Copyright © 2019 Anton Hoang. All rights reserved.
//

import Foundation

protocol ViewModelType {
    associatedtype Input
    associatedtype Output

    func transform(input: Input) -> Output
}

