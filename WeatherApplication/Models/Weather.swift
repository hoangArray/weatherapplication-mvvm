//
//  Weather.swift
//  WeatherApplication
//
//  Created by Anton Hoang on 12/12/2019.
//  Copyright © 2019 Anton Hoang. All rights reserved.
//

import Foundation


struct Weather: Codable {
    
    var latitude: Double
    var longitude: Double
    var timezone: String
    var currently: Current
    var hourly: Hourly
    var daily: Daily
    
    enum CodingKeys: String, CodingKey {
        case latitude, longitude, timezone, currently, daily, hourly
    }

    init(latitude: Double,
          longitude: Double,
          timezone: String,
          currently: Current,
          hourly: Hourly,
          daily: Daily) {

        self.latitude = latitude
        self.longitude = longitude
        self.timezone = timezone
        self.currently = currently
        self.hourly = hourly
        self.daily = daily
    }
    
    
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        latitude = try container.decode(Double.self, forKey: .latitude)
        longitude = try container.decode(Double.self, forKey: .longitude)
        timezone = try container.decode(String.self, forKey: .timezone)
        currently = try container.decode(Current.self, forKey: .currently)
        daily = try container.decode(Daily.self, forKey: .daily)
        hourly = try container.decode(Hourly.self, forKey: .hourly)
        
    }
}

struct Current: Codable {
    var time: Int
    var icon: String
    var temperature: Double
    var summary: String
    
    enum CodingKeys: String, CodingKey {
        case time, icon, temperature, summary
    }
    
    init(time: Int,
         icon: String,
         temperature: Double,
         summary: String) {
        self.time = time
        self.icon = icon
        self.temperature = temperature
        self.summary = summary
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        time = try container.decode(Int.self, forKey: .time)
        icon = try container.decode(String.self, forKey: .icon)
        temperature = try container.decode(Double.self, forKey: .temperature)
        summary = try container.decode(String.self, forKey: .summary)
    }
}

struct Hourly: Codable {
    var summary: String
    var icon: String
    var data: [HourlyDataForeCast]
    
    enum CodingKeys: String, CodingKey {
        case summary, icon, data
    }
    
    init(summary: String,
         icon: String,
         data: [HourlyDataForeCast]) {
        self.summary = summary
        self.icon = icon
        self.data = data
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        summary = try container.decode(String.self, forKey: .summary)
        icon = try container.decode(String.self, forKey: .icon)
        data = try container.decode([HourlyDataForeCast].self, forKey: .data)
    }
}

struct HourlyDataForeCast: Codable {
    
    var temperature: Double
    var icon: String
    
    enum CodingKeys: String, CodingKey {
        case temperature, icon
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        temperature = try container.decode(Double.self, forKey: .temperature)
        icon = try container.decode(String.self, forKey: .icon)
    }
}

struct Daily: Codable {
    
    var icon: String
    var data: [DailyForecastData]
    
    enum CodingKeys: String, CodingKey {
        case icon, data
    }
    
    init(icon: String,
         data: [DailyForecastData]) {
        self.icon = icon
        self.data = data
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        icon = try container.decode(String.self, forKey: .icon)
        data = try container.decode([DailyForecastData].self, forKey: .data)
    }
}

struct DailyForecastData: Codable {
    var time: Int
    var icon: String
    var summary: String
    var temperatureMin: Double
    var temperatureMax: Double
    var pressure: Double
    var windSpeed: Double
    var visibility: Double
    
    enum CodingKeys: String, CodingKey {
        case time,
        icon,
        summary,
        temperatureMin,
        temperatureMax,
        pressure,
        windSpeed,
        visibility
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        time = try container.decode(Int.self, forKey: .time)
        icon = try container.decode(String.self, forKey: .icon)
        summary = try container.decode(String.self, forKey: .summary)
        temperatureMin = try container.decode(Double.self, forKey: .temperatureMin)
        temperatureMax = try container.decode(Double.self, forKey: .temperatureMax)
        pressure = try container.decode(Double.self, forKey: .pressure)
        windSpeed = try container.decode(Double.self, forKey: .windSpeed)
        visibility = try container.decode(Double.self, forKey: .visibility)
    }
}
