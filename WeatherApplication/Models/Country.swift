//
//  Country.swift
//  WeatherApplication
//
//  Created by Anton Hoang on 06/12/2019.
//  Copyright © 2019 Anton Hoang. All rights reserved.
//

import Foundation

struct Country: Codable {
    
    let ukraine: [String]
    
    enum CodingKeys: String, CodingKey {
        case ukraine = "Ukraine"
    }
    
    init(country: [String]) {
        self.ukraine = country
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        ukraine = try container.decode([String].self, forKey: .ukraine)
    }
}
