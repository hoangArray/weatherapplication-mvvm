//
//  SwitchState.swift
//  WeatherApplication
//
//  Created by Anton Hoang on 1/24/20.
//  Copyright © 2020 Anton Hoang. All rights reserved.
//

import Foundation

struct SwitchState {
    var pressure: Bool
    var windSpeed: Bool
    var visibility: Bool
}

