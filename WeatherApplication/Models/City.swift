//
//  Model.swift
//  WeatherApplication
//
//  Created by Anton Hoang on 24/11/2019.
//  Copyright © 2019 Anton Hoang. All rights reserved.
//

import Foundation
import CoreData
import RxDataSources

class City : ObjectConvertible {
    
    var identifier: String?
    var name: String
    var temp: Double
    
    init(name: String, temp: Double, identifier: String? = nil) {
        self.name = name
        self.temp = temp
        self.identifier = identifier
    }
}
