//
//  DailyViewModel.swift
//  WeatherApplication
//
//  Created by Anton Hoang on 12/23/19.
//  Copyright © 2019 Anton Hoang. All rights reserved.
//

import Foundation
import RxSwift

class DetailDailyForecastViewModel {
    
    //MARK: - Properties
    private var this: CommonViewModel
    var forecastSettingsStore: ForecastSettingsStoreProtocol
    var cityName: String?
    var detailDailyData: Observable<DailyForecastData>?
    
    init(this: CommonViewModel) {
        self.this = this
        self.forecastSettingsStore = this.forecastSettingsStore
    }
    
    func fetchSwitchStates() -> [Bool] {
        return self.forecastSettingsStore.fetchSwitchStates()
    }
    
    func fetchSwitchStatesObserver() -> Observable<SwitchState> {
        return self.forecastSettingsStore.fetchSwitchStatesObserver()
    }
    
    func fetchAllForecastSettings() -> Observable<[ForecastSettings]> {
        return self.forecastSettingsStore.fetchAllForecastSettings()
    }
}
