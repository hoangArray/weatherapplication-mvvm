//
//  DailyForecastViewController.swift
//  WeatherApplication
//
//  Created by Anton Hoang on 12/23/19.
//  Copyright © 2019 Anton Hoang. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class DetailDailyForecastViewController: UIViewController, ViewModelBindableType {
    
    //MARK: - IBOutlets
    @IBOutlet weak var cityTitle: UILabel!
    @IBOutlet weak var currentDailyDate: UILabel!
    @IBOutlet weak var forecastImage: UIImageView!
    @IBOutlet weak var forecastLabel: UILabel!
    @IBOutlet weak var temperatureLabel: UILabel!
    @IBOutlet weak var scroll: UIScrollView!
    @IBOutlet weak var dailyView: UIView!
    @IBOutlet weak var pressureLabel: UILabel!
    @IBOutlet weak var windSpeedLabel: UILabel!
    @IBOutlet weak var visibilityLabel: UILabel!
    
    //MARK: - Propeties
    let isControlHidden = BehaviorRelay<Bool>(value: true)
    var viewModel: DetailDailyForecastViewModel!
    let disposeBag = DisposeBag()
    
    //MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        bindViewModel()
    }
    
    private func setupUI() {
        view.backgroundColor = UIColor(red: 0/255, green: 136/255, blue: 150/255, alpha: 1)
        dailyView.backgroundColor = .clear
        pressureLabel.textColor = .white
        windSpeedLabel.textColor = .white
        visibilityLabel.textColor = .white
        
        cityTitle.textColor = .white
        cityTitle.font = UIFont.preferredFont(forTextStyle: .title1)
        
        temperatureLabel.font = UIFont.boldSystemFont(ofSize: 60)
        temperatureLabel.textColor = .white
        temperatureLabel.backgroundColor = .none
        
        currentDailyDate.textColor = .white
        currentDailyDate.font = UIFont.preferredFont(forTextStyle: .body)
        
        forecastLabel.textColor = .white
        forecastLabel.numberOfLines = 0
        forecastLabel.translatesAutoresizingMaskIntoConstraints = false
        forecastImage.contentMode = .scaleAspectFit
    }
    
    private func setupLayout() {
        //MARK:- add city title constraints
        NSLayoutConstraint.activate([
            cityTitle.topAnchor.constraint(equalTo: view.topAnchor, constant: 50),
            cityTitle.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            cityTitle.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            cityTitle.centerXAnchor.constraint(equalTo: view.centerXAnchor)
        ])
        
        //MARK:- add current daily date constraints
        NSLayoutConstraint.activate([
            currentDailyDate.topAnchor.constraint(equalTo: cityTitle.bottomAnchor),
            currentDailyDate.leadingAnchor.constraint(equalTo: cityTitle.leadingAnchor),
            currentDailyDate.trailingAnchor.constraint(equalTo: cityTitle.trailingAnchor),
            currentDailyDate.centerXAnchor.constraint(equalTo: view.centerXAnchor)
        ])
        
        //MARK:- add forecast image constraints
        NSLayoutConstraint.activate([
            forecastImage.topAnchor.constraint(equalTo: currentDailyDate.bottomAnchor),
            forecastImage.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 100),
            forecastImage.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -100),
            forecastImage.centerXAnchor.constraint(equalTo: view.centerXAnchor)
        ])
        
        //MARK:- add forecast label constraints
        NSLayoutConstraint.activate([
            forecastLabel.topAnchor.constraint(equalTo: forecastImage.bottomAnchor),
            forecastLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            forecastLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 50),
            forecastLabel.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -50)
        ])
        
        //MARK:- add temperature label  constraints
        NSLayoutConstraint.activate([
            temperatureLabel.topAnchor.constraint(equalTo: forecastLabel.bottomAnchor),
            temperatureLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            temperatureLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 50),
            temperatureLabel.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -50),
            temperatureLabel.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -150)
        ])
    }
    
    func bindViewModel() {
        cityTitle.text = viewModel.cityName
        let daily = viewModel.detailDailyData?.share(replay: 1,
                                                     scope: .whileConnected)
        
        daily?.map{
            return "\(Int($0.temperatureMin))°C"
        }
        .asDriver(onErrorJustReturn: "")
        .drive(temperatureLabel.rx.text)
        .disposed(by: disposeBag)
        
        daily?.map { [unowned self] in
            self.forecastImage.rx.image.onNext(UIImage(named: $0.icon))
        }.asDriver(onErrorJustReturn: ())
            .drive()
            .disposed(by: disposeBag)
        
        daily?.map {
            return "\($0.summary)"
        }
        .asDriver(onErrorJustReturn: "")
        .drive(forecastLabel.rx.text)
        .disposed(by: disposeBag)
        
        daily?.map {
            let date = Date(timeIntervalSince1970: TimeInterval($0.time))
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "EEEE"
            let localeDate = dateFormatter.string(from: date)
            
            return localeDate
        }
        .asDriver(onErrorJustReturn: "")
        .drive(currentDailyDate.rx.text)
        .disposed(by: disposeBag)
        
        daily?.map {
            return "Pressure \n\n\($0.pressure) Pa"
        }
        .asDriver(onErrorJustReturn: "")
        .drive(pressureLabel.rx.text)
        .disposed(by: disposeBag)
        
        daily?.map {
            return "WindSpeed \n\n\($0.windSpeed) m/s"
        }
        .asDriver(onErrorJustReturn: "")
        .drive(windSpeedLabel.rx.text)
        .disposed(by: disposeBag)
        
        daily?.map {
            return "Visibility \n\n\($0.visibility) km"
        }
        .asDriver(onErrorJustReturn: "")
        .drive(visibilityLabel.rx.text)
        .disposed(by: disposeBag)
        
        let switchState = self.isControlHidden
            .flatMapLatest { [unowned self] _ in
                return self.viewModel.fetchSwitchStatesObserver()
        }
        .asDriver(onErrorJustReturn: SwitchState(pressure: false, windSpeed: false, visibility: false))
        
        switchState.map { !$0.pressure }
            .drive(pressureLabel.rx.isHidden)
            .disposed(by: disposeBag)
        
        switchState.map { !$0.windSpeed }
            .drive(windSpeedLabel.rx.isHidden)
            .disposed(by: disposeBag)
        
        switchState.map { !$0.visibility }
            .drive(visibilityLabel.rx.isHidden)
            .disposed(by: disposeBag)
    }
}
