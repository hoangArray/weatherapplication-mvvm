//
//  MainPresenter.swift
//  WeatherApplication
//
//  Created by Anton Hoang on 24/11/2019.
//  Copyright © 2019 Anton Hoang. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import RxSwiftExt
import Action

class ListOfCitiesViewModel {
    
    //MARK: - Properties
    private var this: CommonViewModel
    var cityStore: CityStorable
    var sceneCoordinator: SceneCoordinator
    var networkUseCase: NetworkManagerUseCase
    
    init(this: CommonViewModel) {
        self.this = this
        self.cityStore = this.cityStore
        self.sceneCoordinator = this.sceneCoordinator
        self.networkUseCase = this.networkUseCase
    }
    
    func fetchAllObjects() -> Observable<[City]>{
        return self.cityStore.fetchAllObjects()
    }
    
    func performAddCityViewController() -> CocoaAction {
        
        return CocoaAction { _ in
            let addCityViewModel = FindCityViewModel(this: self.this)
            let addCityScene = Scene.findCity(addCityViewModel)
            return self.sceneCoordinator.transition(to: addCityScene,
                                                    with: .push,
                                                    animated: true).asObservable().map({ _ in })
        }
    }
    
    func performCityForecast() -> AnyObserver<String>  {
        return AnyObserver.init(eventHandler: { [unowned self] (event) in
            switch event {
            case .next(let value):
                let forecastVM = DailyForecastViewModel(this: self.this)
                forecastVM.cityName = value
                
                let data = self.networkUseCase.updateWeatherForLocation(location: value)
                forecastVM.dailyForecastData = data.map({ $0.daily.data })
                forecastVM.selectedLocation = value
                
                let cityForecastScene = Scene.dailyForecast(forecastVM)
                self.sceneCoordinator.transition(to: cityForecastScene,
                                                 with: .push,
                                                 animated: true)
                
            default: break
            }
        })
    }
}
