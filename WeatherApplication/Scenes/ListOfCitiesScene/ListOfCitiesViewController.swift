//
//  ViewController.swift
//  WeatherApplication
//
//  Created by Anton Hoang on 24/11/2019.
//  Copyright © 2019 Anton Hoang. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import RxDataSources
import CoreData


class ListOfCitiesViewController: UIViewController, ViewModelBindableType {
    
    //MARK: - IBOutlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var addCityButton: UIBarButtonItem!
            
    //MARK: - Properties
    var viewModel: ListOfCitiesViewModel!
    private let disposeBag = DisposeBag()
    typealias CityModel = SectionModel<String, City>
    var cities = BehaviorRelay<[City]>(value: [])
        
    //MARK: - lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        bindViewModel()
        setupUI()
    }
    
    private func setupUI() {
        title = "Your cities"
        self.navigationItem.hidesBackButton = true
    }

    //MARK: - Bindings
    func bindViewModel() {
        self.rx.methodInvoked(#selector(viewWillAppear(_:)))
            .throttle(RxTimeInterval.milliseconds(500),
                      scheduler: MainScheduler.instance)
            .flatMapLatest { _ in
                self.viewModel.fetchAllObjects()
            }
            .asDriver(onErrorJustReturn: [])
            .drive(cities)
            .disposed(by: disposeBag)
        
        cities
            .asDriver()
            .drive(tableView.rx.items(cellIdentifier: "cell",
                                      cellType: UITableViewCell.self)) { ind, el, cell in
                cell.textLabel?.text = el.name
                cell.selectionStyle = .none
        }
        .disposed(by: disposeBag)
                
        tableView.rx.itemSelected.asDriver()
            .map { [unowned self] indexPath in self.cities.value[indexPath.row].name }
            .asObservable()
            .bind(to: viewModel.performCityForecast())
            .disposed(by: disposeBag)
        
        tableView.rx.itemDeleted
            .subscribe { [unowned self] indexPath in
                let ip = indexPath.element
                guard let ipElement = ip else { return }
                
                let city = self.cities.value[ipElement.row]
                self.viewModel.cityStore.delete(city)
                self.cities.remove(at: ipElement.row)
        }
        .disposed(by: disposeBag)
        
        addCityButton.rx.action = viewModel.performAddCityViewController()
    }
}


