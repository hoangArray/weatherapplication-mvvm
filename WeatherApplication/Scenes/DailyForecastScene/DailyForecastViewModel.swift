//
//  CityForecastViewModel.swift
//  WeatherApplication
//
//  Created by Anton Hoang on 01/12/2019.
//  Copyright © 2019 Anton Hoang. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import Action

class DailyForecastViewModel {
    
    //MARK: - Properties
    var selectedLocation: String?
    var dailyForecastData: Observable<[DailyForecastData]>?
    var cityName: String?
    var networkManager: NetworkManagerUseCase
    var sceneCoordinator: SceneCoordinator
    private var this: CommonViewModel
    
    init(this: CommonViewModel) {
        self.this = this
        self.networkManager = this.networkUseCase
        self.sceneCoordinator = this.sceneCoordinator
    }
    
    func performMapByLocation() -> CocoaAction {
        
        return CocoaAction { [unowned self] _ in
            let mapViewModel = MapViewModel()
            let scene = Scene.mapScreen(mapViewModel)
            return self.sceneCoordinator.transition(to: scene, with: .push, animated: true).asObservable().map({ _ in})
        }
    }
    
    func performDetailDailyViewController() -> AnyObserver<IndexPath> {
        
        return AnyObserver.init { [unowned self] (event) in
            switch event {
            case .next(let indexPath):
                let detailDailyVM = DetailDailyForecastViewModel(this: self.this)
                detailDailyVM.cityName = self.cityName
                
                let detailDaily = self.networkManager
                    .updateWeatherForLocation(location: self.cityName ?? "")
                detailDailyVM.detailDailyData = detailDaily.map { $0.daily.data[indexPath.section] }
                
                let detailDailyScene = Scene.detailDailyScreen(detailDailyVM)
                self.sceneCoordinator.transition(to: detailDailyScene, with: .push, animated: true)
            default: break
            }
        }
    }
    
    func performSettingsForecastViewController() -> CocoaAction {
        
        return CocoaAction { [unowned self] _ in
            let settingsVM = SettingsForecastViewModel(this: self.this)
            let settingsScene = Scene.settingsForecastScreen(settingsVM)
            return self.sceneCoordinator.transition(to: settingsScene, with: .push, animated: true).asObservable().map { _ in }
        }
    }
}

