//
//  CityForecastViewController.swift
//  WeatherApplication
//
//  Created by Anton Hoang on 01/12/2019.
//  Copyright © 2019 Anton Hoang. All rights reserved.
//

import UIKit
import Foundation
import RxSwift
import RxCocoa
import RxDataSources

class DailyForecastViewController: UIViewController, ViewModelBindableType {
    
    //MARK: - IBOutlets
    @IBOutlet weak var tableView: UITableView!
    
    //MARK: - Properties
    var viewModel: DailyForecastViewModel!
    private let disposeBag = DisposeBag()
    var selectedLocation: String?
    typealias DataModel = SectionModel<String, DailyForecastData>
    var dailyForecastData = BehaviorRelay<[DailyForecastData]>(value: [])
    
    //MARK: - lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        bindViewModel()
    }
    
    lazy var segueToSetting: UIBarButtonItem = {
        let settings = UIBarButtonItem(title: "Settings", style: .plain, target: self, action: nil)
        return settings
    }()
    
    lazy var showCityButton: UIView = {
        let buttonView = UIView(frame: CGRect(x: 0, y: 0, width: 50, height: 50))
        buttonView.backgroundColor = .red
        return buttonView
    }()
    
    lazy var showCityOnMap: UIBarButtonItem = {
        let city = UIBarButtonItem(barButtonSystemItem: .search, target: self, action: nil)
        return city
    }()
    
    func setupUI() {
        title = viewModel.cityName
        navigationItem.rightBarButtonItems = [showCityOnMap, segueToSetting]
    }
    
    //MARK: - Bindings
    func bindViewModel() {
        
        showCityOnMap.rx.action = viewModel.performMapByLocation()
        segueToSetting.rx.action = viewModel.performSettingsForecastViewController()
        
        viewModel.dailyForecastData?.asObservable()
            .observeOn(MainScheduler.instance)
            .bind(to: dailyForecastData)
            .disposed(by: disposeBag)
        
        let dataSource = RxTableViewSectionedReloadDataSource<DataModel>(configureCell: {
            ds, tv, ip, i in
            let cell = tv.dequeueReusableCell(withIdentifier: "cell", for: ip)
            let minT = "\(i.temperatureMin)"
            let maxT = "\(i.temperatureMax)"
            cell.textLabel?.text = minT + " ~~ " + maxT
            cell.imageView?.image = UIImage(named: i.icon)
            cell.detailTextLabel?.text = i.summary
            
            return cell
            
        }, titleForHeaderInSection: { ds, index in
            let date = Calendar.current.date(byAdding: .day, value: index, to: Date())
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "MMMM dd, yyyy"
            return dateFormatter.string(from: date!)
        })
        
        dailyForecastData
            .map {
            return $0.map {
                item in SectionModel(model: "", items: [item])
            }
        }
        .bind(to: self.tableView.rx.items(dataSource: dataSource))
        .disposed(by: disposeBag)
        
        tableView.rx
            .itemSelected
            .map { indexPath in
                self.tableView.deselectRow(at: indexPath, animated: true)
                return indexPath
        }
        .bind(to: viewModel.performDetailDailyViewController())
        .disposed(by: disposeBag)
        
        tableView.rx
            .setDelegate(self)
            .disposed(by: disposeBag)
    }
}

extension DailyForecastViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int){
        view.tintColor = .white
        let header: UITableViewHeaderFooterView = view as! UITableViewHeaderFooterView
        header.textLabel?.textColor = .black
    }
}
