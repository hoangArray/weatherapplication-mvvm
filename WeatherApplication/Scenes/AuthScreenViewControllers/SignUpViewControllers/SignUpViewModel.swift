//
//  LoginViewModel.swift
//  WeatherApplication
//
//  Created by Anton Hoang on 12/18/19.
//  Copyright © 2019 Anton Hoang. All rights reserved.
//

import Foundation

class SignUpViewModel: CoordinatorHelper {
    
    //MARK: - Properties
    private var this: CommonViewModel
    private var sceneCoordinator: SceneCoordinator
    var firebaseAuthManager: FirebaseAuthProviderUseCase

    init(this: CommonViewModel) {
        self.this = this
        self.sceneCoordinator = this.sceneCoordinator
        self.firebaseAuthManager = this.firebaseAuthManager
    }
    
    func performListOfCities() {
        let listOfCitiesViewModel = ListOfCitiesViewModel(this: self.this)
        let scene = Scene.listOfCities(listOfCitiesViewModel)
        self.sceneCoordinator.transition(to: scene, with: .push, animated: true)
    }
}
