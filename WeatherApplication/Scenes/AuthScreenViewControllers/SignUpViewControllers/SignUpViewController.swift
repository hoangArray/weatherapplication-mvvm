//
//  LoginViewController.swift
//  WeatherApplication
//
//  Created by Anton Hoang on 12/18/19.
//  Copyright © 2019 Anton Hoang. All rights reserved.
//

import UIKit
import RxSwift


class SignUpViewController: UIViewController, ViewModelBindableType {    

    //MARK: - IBOutlets
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    //MARK: - Properties
    var viewModel: SignUpViewModel!
    let disposeBag = DisposeBag()
    
    //MARK: - lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    func setupUI() {
        navigationController?.navigationBar.barTintColor = .clear
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        self.passwordTextField.isSecureTextEntry = true
        let image = UIImage(named: "unsplash5")!
        view.backgroundColor = UIColor(patternImage: image)
        view.contentMode = .scaleAspectFill
    }
    
    func bindViewModel() {}
    
    @IBAction func signUpTapped(_ sender: Any) {
        guard let email = emailTextField.text else { return }
        guard let password = passwordTextField.text else { return }
        
        viewModel
            .firebaseAuthManager
            .createUser(email: email, password: password) { [unowned self] (success) in
                success.map {
                    if $0 {
                        self.viewModel.performListOfCities()
                    } else {
                        print("fails")
                    }}
                    .subscribe { _ in }
                    .disposed(by: self.disposeBag)
        }
    }
}
