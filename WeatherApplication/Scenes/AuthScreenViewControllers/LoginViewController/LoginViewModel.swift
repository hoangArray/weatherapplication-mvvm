//
//  LoginViewModel.swift
//  WeatherApplication
//
//  Created by Anton Hoang on 12/19/19.
//  Copyright © 2019 Anton Hoang. All rights reserved.
//

import Foundation

class LoginViewModel: CoordinatorHelper {
    
    //MARK: - Properties
    private var this: CommonViewModel
    private var sceneCoordinator: SceneCoordinator
    var firebaseAuthManager: FirebaseAuthProviderUseCase
    
    init(this: CommonViewModel) {
        self.this = this
        self.sceneCoordinator = this.sceneCoordinator
        self.firebaseAuthManager = this.firebaseAuthManager
    }
    
    func performListOfCities() {
        let listCityVM = ListOfCitiesViewModel(this: self.this)
        let scene = Scene.listOfCities(listCityVM)
        self.sceneCoordinator.transition(to: scene, with: .push, animated: true)
    }
}
