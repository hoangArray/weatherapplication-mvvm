//
//  LoginViewController.swift
//  WeatherApplication
//
//  Created by Anton Hoang on 12/19/19.
//  Copyright © 2019 Anton Hoang. All rights reserved.
//

import UIKit
import RxSwift

class LoginViewController: UIViewController, ViewModelBindableType {

    //MARK: - IBOutlets
    @IBOutlet weak var emailLoginText: UITextField!
    @IBOutlet weak var passwordLoginText: UITextField!
    
    //MARK: - Properties
    var viewModel: LoginViewModel!
    let disposeBag = DisposeBag()
    
    //MARK: - lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    func bindViewModel() {}
    
    private func setupUI() {
        let image = UIImage(named: "unsplash4")!
        view.backgroundColor = UIColor(patternImage: image)
        view.contentMode = .scaleAspectFill
    }
    
    @IBAction func logInTapped(_ sender: Any) {
        guard let email = emailLoginText.text else { return }
        guard let password = passwordLoginText.text else { return }
        
        viewModel.firebaseAuthManager.signIn(email: email, password: password) { [unowned self] (success) in
            success.map { [unowned self] in
                if $0 {
                    self.viewModel.performListOfCities()
                } else {
                    print("false")
                }}
                .subscribe { _ in }
                .disposed(by: self.disposeBag)
        }
    }
}
