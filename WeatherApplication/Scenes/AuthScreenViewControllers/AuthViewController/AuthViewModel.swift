//
//  AuthViewModel.swift
//  WeatherApplication
//
//  Created by Anton Hoang on 12/19/19.
//  Copyright © 2019 Anton Hoang. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import Action

@available(iOS 13.0, *)
class AuthViewModel {
    
    //MARK: - Properties
    private var this: CommonViewModel
    var sceneCoordinator: SceneCoordinator
    
    init(this: CommonViewModel) {
        self.this = this
        self.sceneCoordinator = this.sceneCoordinator
    }
    
    func performSignUpViewController() -> CocoaAction {
        return CocoaAction { _ in
            let signUpViewModel = SignUpViewModel(this: self.this)
            let scene = Scene.signUpScreen(signUpViewModel)
            return self.sceneCoordinator.transition(to: scene, with: .push, animated: true).asObservable().map({ _ in })
        }
    }
    
    func performLogInViewController() -> CocoaAction {
        return CocoaAction { _ in
            let logInViewModel = LoginViewModel(this: self.this)
            let scene = Scene.logInScreen(logInViewModel)
            return self.sceneCoordinator.transition(to: scene, with: .push, animated: true).asObservable().map({ _ in})
        }
    }
}
