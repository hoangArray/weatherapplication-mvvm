//
//  AuthViewController.swift
//  WeatherApplication
//
//  Created by Anton Hoang on 12/19/19.
//  Copyright © 2019 Anton Hoang. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class AuthViewController: UIViewController, ViewModelBindableType {
    
    //MARK: - IBOutlets
    @IBOutlet weak var signUpButton: UIButton!
    @IBOutlet weak var logInButton: UIButton!
    
    //MARK: - Properties
    var viewModel: AuthViewModel!
    
    //MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        setupLayouts()
        bindViewModel()
    }
    
    func setupUI() {
        let image = UIImage(named: "unsplash4")
        let imageView = UIImageView(image: image)
        imageView.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
        imageView.contentMode = .scaleAspectFill
        view.addSubview(imageView)
        
        navigationController?.navigationBar.prefersLargeTitles = true
        navigationController?.navigationBar.backgroundColor = .none
        signUpButton.backgroundColor = .clear
        signUpButton.setTitleColor(.white, for: .normal)
        signUpButton.frame = CGRect(x: 0, y: 0, width: 100, height: 40)
        signUpButton.translatesAutoresizingMaskIntoConstraints = false
        
        logInButton.backgroundColor = .clear
        logInButton.setTitleColor(.white, for: .normal)
        logInButton.frame = CGRect(x: 0, y: 0, width: 100, height: 40)
        logInButton.translatesAutoresizingMaskIntoConstraints = false
        
        view.addSubview(signUpButton)
        view.addSubview(logInButton)
    }
    
    private func setupLayouts() {
        
        NSLayoutConstraint.activate([
            signUpButton.centerXAnchor.constraint(equalTo: self.view.centerXAnchor),
            signUpButton.centerYAnchor.constraint(equalTo: self.view.centerYAnchor, constant: (self.view.center.y / 2))
        ])
        NSLayoutConstraint.activate([
            logInButton.topAnchor.constraint(equalTo: signUpButton.bottomAnchor),
            logInButton.centerXAnchor.constraint(equalTo: self.view.centerXAnchor),
            logInButton.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: -(self.view.center.y / 3))
        ])
    }
    
    func bindViewModel() {
        signUpButton.rx.action = viewModel.performSignUpViewController()
        logInButton.rx.action = viewModel.performLogInViewController()
    }
    
}
