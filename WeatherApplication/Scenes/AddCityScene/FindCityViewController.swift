//
//  AddCityViewController.swift
//  WeatherApplication
//
//  Created by Anton Hoang on 30/11/2019.
//  Copyright © 2019 Anton Hoang. All rights reserved.

import UIKit
import RxSwift
import RxCocoa
import RxDataSources

class FindCityViewController: UIViewController, ViewModelBindableType {
    
    //MARK: - IBOutlets
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var saveButton: UIBarButtonItem!
    
    //MARK: - Properties
    var viewModel: FindCityViewModel!
    let disposeBag = DisposeBag()

    //MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        bindViewModel()
    }
    
    private func setupUI() {
        title = "Find city"
        tableView.allowsMultipleSelection = true
    }
    
    //MARK: - Bindings
    func bindViewModel() {
        let searchTextDriver: Driver<String?> = searchBar.rx.text.skip(1)
            .distinctUntilChanged()
            .throttle(RxTimeInterval.milliseconds(500), scheduler: MainScheduler.instance)
            .asDriver(onErrorJustReturn: nil)

        let input = FindCityViewModel.Input(searchBarTrigger: searchTextDriver)
        let output = viewModel.transform(input: input)

        output
            .foundedCity
            .drive(tableView.rx.items(cellIdentifier: "cell", cellType: UITableViewCell.self)) { ind, element, cell in
                cell.textLabel?.text = element
                cell.selectionStyle = .none
        }.disposed(by: disposeBag)
        
        let foundedCity = output.foundedCity.asDriver()

        tableView.rx
            .itemSelected.asDriver()
            .map { [unowned self] ip in
                self.tableView.cellForRow(at: ip)?.accessoryType = .checkmark
                self.viewModel.addItem(index: ip.row, foundedCity: foundedCity)
        }
        .drive()
        .disposed(by: disposeBag)
        
        tableView.rx
            .itemDeselected
            .asDriver()
            .map{ [unowned self] ip in
                self.tableView.cellForRow(at: ip)?.accessoryType = .none
                self.viewModel.deleteItem(index: ip.row, foundedCity: foundedCity)
        }
        .drive()
        .disposed(by: disposeBag)
        
        saveButton.rx.action = viewModel.saveItems()
    }
}

extension BehaviorRelay where Element: RangeReplaceableCollection {
    
    func append(_ subElement: Element.Element) {
        var newValue = value
        newValue.append(subElement)
        accept(newValue)
    }
    
    func remove(at index: Element.Index) {
        var newValue = value
        newValue.remove(at: index)
        accept(newValue)
    }
    
    func removeAll() {
        var newValue = value
        newValue.removeAll()
        accept(newValue)
    }
}
