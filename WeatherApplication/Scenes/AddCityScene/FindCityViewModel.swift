//
//  AddCityViewModel.swift
//  WeatherApplication
//
//  Created by Anton Hoang on 30/11/2019.
//  Copyright © 2019 Anton Hoang. All rights reserved.
//

import Foundation
import RxSwift
import Action
import RxCocoa
import CoreData

class FindCityViewModel {
    
    //MARK: - Properties
    let disposeBag = DisposeBag()
    var persistentManager = PersistentManager()
    let listOfCities = BehaviorRelay<[String]>(value: [])
    var searchText: Driver<String>?
    var findCityUseCase: FindCityProviderUseCase
    var cityStore: CityStorable
    var sceneCoordinator: SceneCoordinator
      
    init(this: CommonViewModel) {
        self.findCityUseCase = this.findCityUseCase
        self.cityStore = this.cityStore
        self.sceneCoordinator = this.sceneCoordinator
    }
    
    func addItem(index: Int, foundedCity: Driver<[String]>) {
        
        let filtered = foundedCity.map { $0[index] }
        filtered.map { [unowned self] in
            self.listOfCities.append($0)
        }
        .drive()
        .disposed(by: disposeBag)
    }
    
    func deleteItem(index: Int, foundedCity: Driver<[String]>) {
        foundedCity.map { [unowned self] element in
            let selectedCity = element[index]
            let selectedIndex = self.listOfCities.value.firstIndex(where: {
                $0 == selectedCity
            })
            guard let ind = selectedIndex else { return }
            self.listOfCities.remove(at: ind)
        }
        .drive()
        .disposed(by: disposeBag)
    }
}

extension FindCityViewModel: ViewModelType {
    struct Input {
        let searchBarTrigger: Driver<String?>
    }

    struct Output {
        let foundedCity: Driver<[String]>
    }
    
    func transform(input: Input) -> Output {
        let city = input
            .searchBarTrigger.asObservable()
            .flatMapLatest( { [unowned self] element in
            return self.findCityUseCase.findCities(element ?? "")
        })
        
        return .init(foundedCity: city.asDriver(onErrorJustReturn: []))
    }
    
    func saveItems() -> CocoaAction {
        
        return CocoaAction { [unowned self] _ in
            self.listOfCities.map{ $0.forEach { [unowned self] element in
                let city = City(name: element, temp: 0)
                self.cityStore.insert(city)
                }
            }
            .subscribe { _ in }
            .disposed(by: self.disposeBag)
            
            return self.sceneCoordinator.close(animated: true).asObservable().map({ _ in })
        }
    }
}
