//
//  SettingsUserCell.swift
//  WeatherApplication
//
//  Created by Anton Hoang on 12/26/19.
//  Copyright © 2019 Anton Hoang. All rights reserved.
//

import UIKit

class SettingsUserCell: UITableViewCell {

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
