//
//  SettingsForecastScene.swift
//  WeatherApplication
//
//  Created by Anton Hoang on 12/25/19.
//  Copyright © 2019 Anton Hoang. All rights reserved.
//

import Foundation
import CoreData
import RxSwift

class SettingsForecastViewModel {
    
    private var this: CommonViewModel
    var sceneCoordinator: SceneCoordinator
    var forecastSettingsStore: ForecastSettingsStoreProtocol
    let disposeBag = DisposeBag()
            
    init(this: CommonViewModel) {
        self.this = this
        self.sceneCoordinator = this.sceneCoordinator
        self.forecastSettingsStore = this.forecastSettingsStore
    }
    
    func fetchAllForecastSettings() -> Observable<[ForecastSettings]> {
        self.forecastSettingsStore.fetchAllForecastSettings()
    }
    
    func fetchSelectedDayNumber() -> Int64 {
        var selectedDay: Int64?
        fetchAllForecastSettings()
            .mapMany {
                selectedDay = $0.selectedDay?.dayNumber
        }
        .subscribe { _ in }
        .disposed(by: disposeBag)
        
        return selectedDay ?? 0
    }
    
    func fetchSwitchStates() -> [Bool] {
        return forecastSettingsStore.fetchSwitchStates()
    }

    func performDetailsUserSettingsViewController() {
        let detailUserSettingsVM = DetailUserSettingsViewModel(this: this)
        let detailUserSettingsScene = Scene.detailUserSettingsScreen(detailUserSettingsVM)
        sceneCoordinator.transition(to: detailUserSettingsScene, with: .push, animated: true)
    }
    
    func updateValue(state: Bool, key: String) {
        forecastSettingsStore.updateValue(state: state, key: key) 
    }
}
