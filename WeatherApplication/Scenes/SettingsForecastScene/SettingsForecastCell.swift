//
//  SettingsForecastCell.swift
//  WeatherApplication
//
//  Created by Anton Hoang on 12/26/19.
//  Copyright © 2019 Anton Hoang. All rights reserved.
//

import UIKit

class SettingsForecastCell: UITableViewCell {
    
    var indexTag: Int?
        
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        addSubview(forecastSwitch)
        setupLayout()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupLayout() {
        forecastSwitch.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        forecastSwitch.rightAnchor.constraint(equalTo: rightAnchor, constant: -16).isActive = true
    }
    
    lazy var forecastSwitch: UISwitch = {
       let forecastSwitch = UISwitch()
        forecastSwitch.translatesAutoresizingMaskIntoConstraints = false
        return forecastSwitch
    }()
}
