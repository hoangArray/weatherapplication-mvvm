//
//  SettingsCell.swift
//  WeatherApplication
//
//  Created by Anton Hoang on 12/26/19.
//  Copyright © 2019 Anton Hoang. All rights reserved.
//

import Foundation

enum SettingsSection: Int, CaseIterable, CustomStringConvertible {
    case Forecast
    case User
    
    var height: Float {
        switch self {
        case .Forecast:
            return 60
        case .User:
            return 40
        }
    }
    
    var description: String {
        switch self {
        case .Forecast:
            return "Choose high activity parameteres of forecast."
        case .User:
            return "Use some special settings for user."
        }
    }
}

enum ForecastSection: Int, CaseIterable, CustomStringConvertible {
    case Pressure
    case WindSpeed
    case Visibility
    
    var description: String {
        switch self {
        case .Pressure:
            return "Pressure"
        case .WindSpeed:
            return "WindSpeed"
        case .Visibility:
            return "Visibility"
        }
    }
    
    var descriptionModel: String {
        switch self {
        case .Pressure:
            return "pressure"
        case .WindSpeed:
            return "windSpeed"
        case .Visibility:
            return "visibility"
        }
    }
}

enum UserSection: Int, CaseIterable, CustomStringConvertible {
    case Daily
    var description: String {
        switch self {
        case .Daily:
            return "Daily"
        }
    }
}

