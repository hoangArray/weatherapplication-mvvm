//
//  SettingsForecastViewController.swift
//  WeatherApplication
//
//  Created by Anton Hoang on 12/25/19.
//  Copyright © 2019 Anton Hoang. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import CoreData

class SettingsForecastViewController: UIViewController, ViewModelBindableType {
    
    var viewModel: SettingsForecastViewModel!
    let reuseIdentifier = "cell"
    var tableview = UITableView()
    let disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        //        bindViewModel()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tableview.reloadData()
    }
    
    private func setupUI() {
        
        title = "Settings"
        navigationItem.largeTitleDisplayMode = .never
        tableview.frame = CGRect(x: 0,
                                 y: 0,
                                 width: self.view.frame.width,
                                 height: self.view.frame.height)
        
        tableview.register(SettingsForecastCell.self, forCellReuseIdentifier: "SettingsForecastCell")
        tableview.register(SettingsUserCell.self, forCellReuseIdentifier: "SettingsUserCell")
        
        tableview.dataSource = self
        tableview.delegate = self
        tableview.isScrollEnabled = false
        view.addSubview(tableview)
    }
    
    func bindViewModel() {}
}

extension SettingsForecastViewController: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return SettingsSection.allCases.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        guard let section = SettingsSection(rawValue: section) else { return 0 }
        switch section {
        case .Forecast:
            return ForecastSection.allCases.count
        case .User:
            return UserSection.allCases.count
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UIView()
        view.backgroundColor = UIColor(red: 242/255,
                                       green: 242/255,
                                       blue: 247/255,
                                       alpha: 1)
        
        let title = UILabel()
        title.font = UIFont.boldSystemFont(ofSize: 16)
        title.textColor = .black
        title.numberOfLines = 0
        title.text = SettingsSection(rawValue: section)?.description
        view.addSubview(title)
        
        title.translatesAutoresizingMaskIntoConstraints = false
        title.topAnchor.constraint(equalTo: view.topAnchor, constant: 10).isActive = true
        title.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        title.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 16).isActive = true
        title.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -10).isActive = true
        
        return view
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return CGFloat(SettingsSection(rawValue: section)?.height ?? 0)
    }
    
    @objc func changeState(sender: UISwitch) {
        guard let id = sender.restorationIdentifier else { return }
        viewModel.updateValue(state: sender.isOn, key: id)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let section = SettingsSection(rawValue: indexPath.section) else { return UITableViewCell() }
        
        switch section {
        case .Forecast:
            let forecastCell = tableView.dequeueReusableCell(withIdentifier: "SettingsForecastCell") as! SettingsForecastCell
            forecastCell.textLabel?.text = ForecastSection(rawValue: indexPath.row)?.description
            forecastCell.selectionStyle = .none
            
            let forecast = viewModel.fetchSwitchStates()
            forecastCell.forecastSwitch.isOn = forecast[indexPath.row]
            forecastCell.forecastSwitch.restorationIdentifier = ForecastSection(rawValue: indexPath.row)?.descriptionModel
            forecastCell.forecastSwitch.addTarget(self, action: #selector(changeState(sender:)), for: .valueChanged)
            
            return forecastCell
            
        case .User:
            var userCell = tableView.dequeueReusableCell(withIdentifier: "SettingsUserCell") as! SettingsUserCell
            
            userCell = SettingsUserCell(style: .value1, reuseIdentifier: "SettingsUserCell")
            userCell.textLabel?.text = UserSection(rawValue: indexPath.row)?.description
            userCell.accessoryType = .disclosureIndicator
            userCell.detailTextLabel?.text = "\(viewModel.fetchSelectedDayNumber()) days"
            
            let footerView = UIView()
            footerView.frame = CGRect(x: 0, y: 0,
                                      width: tableView.frame.width,
                                      height: tableView.frame.height)
            footerView.backgroundColor = UIColor(red: 242/255,
                                                 green: 242/255,
                                                 blue: 247/255,
                                                 alpha: 1)
            tableView.tableFooterView = footerView
            
            return userCell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        guard let section = SettingsSection(rawValue: indexPath.section) else { return }
        
        switch section {
        case .User:
            viewModel.performDetailsUserSettingsViewController()
        default: break
        }
    }
}


//}
