//
//  MapViewController.swift
//  WeatherApplication
//
//  Created by Anton Hoang on 12/22/19.
//  Copyright © 2019 Anton Hoang. All rights reserved.
//

import UIKit
import MapKit
import RxSwift
import CoreLocation

class MapViewController: UIViewController, ViewModelBindableType {

    //MARK: - IBOutlets
    @IBOutlet weak var mapView: MKMapView!
    //MARK: - Properties
    let locationManager = CLLocationManager()
    var viewModel: MapViewModel!
    
    //MARK: - lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        bindViewModel()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        checkLocationEnabled()
    }
    
    private func setupUI() {
        navigationItem.largeTitleDisplayMode = .never
        mapView.delegate = self
    }
    
    private func setupLocationManager() {
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
    }
    
    private func checkLocationEnabled() {
        if CLLocationManager.locationServicesEnabled() {
            setupLocationManager()
            checkAuthorization()
        } else {
            showAlertLocation(title: "Location is unabled", message: "Do you want to turn on?", url: URL(string: "App-Prefs:root=LOCATION_SERVICES"))
        }
    }
    
    private func checkAuthorization() {
        switch CLLocationManager.authorizationStatus() {
        case .authorizedAlways:
            break
        case .authorizedWhenInUse:
            mapView.showsUserLocation = true
            locationManager.startUpdatingLocation()
            break
        case .denied:
            showAlertLocation(title: "Location was denied", message: "Do you want to change?", url: URL(string: UIApplication.openSettingsURLString))
            break
        case .restricted:
            break
        case .notDetermined:
            locationManager.requestWhenInUseAuthorization()
        default:
            break
        }
    }
    
    private func showAlertLocation(title: String, message: String?, url: URL?) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let settings = UIAlertAction(title: "Settings", style: .default) { (alert) in
            if let url = url {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
        }
        
        let cancelButtom = UIAlertAction(title: "No, thanks", style: .cancel, handler: nil)
        alert.addAction(cancelButtom)
        alert.addAction(settings)
        present(alert, animated: true, completion: nil)
    }
    
    func bindViewModel() {}
}

extension MapViewController: MKMapViewDelegate {}

extension MapViewController: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.last?.coordinate {
            let region = MKCoordinateRegion(center: location, latitudinalMeters: 500, longitudinalMeters: 500)
            mapView.setRegion(region, animated: true)
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        checkAuthorization()
    }
}
