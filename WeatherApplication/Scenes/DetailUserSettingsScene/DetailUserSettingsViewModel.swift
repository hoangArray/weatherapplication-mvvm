//
//  DetailUserSettingsViewModel.swift
//  WeatherApplication
//
//  Created by Anton Hoang on 12/26/19.
//  Copyright © 2019 Anton Hoang. All rights reserved.
//

import Foundation
import UIKit
import RxSwift

class DetailUserSettingsViewModel {
        
    private var this: CommonViewModel
        
    var dailySettingsStore: DailySettingsStorable
    
    var forecastSettingsStore: ForecastSettingsStoreProtocol

    var sceneCoordinator: SceneCoordinator
        
    var disposeBag = DisposeBag()
    
    init(this: CommonViewModel) {
        self.this = this
        self.dailySettingsStore = this.dailySettingsStore
        self.sceneCoordinator = this.sceneCoordinator
        self.forecastSettingsStore = this.forecastSettingsStore
    }
    
    func dailySettingsList() -> Observable<[Day?]> {
        return dailySettingsStore.fetchAllDayObjects()
    }
    
    func getForecastSettings() -> Day? {
        weak var day: Day?
        forecastSettingsStore.fetchAllForecastSettings()
            .mapMany { day = $0.selectedDay } 
            .subscribe { _ in }
            .disposed(by: disposeBag)
        return day
    }
    
    func update(day: Day?, forecast: ForecastSettings?) {
        dailySettingsStore.update(day: day, forecast: forecast)
    }
}
