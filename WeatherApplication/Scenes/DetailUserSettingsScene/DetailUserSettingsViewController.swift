//
//  DetailUserSettingsViewControllers.swift
//  WeatherApplication
//
//  Created by Anton Hoang on 12/29/19.
//  Copyright © 2019 Anton Hoang. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import RxDataSources

class DetailUserSettingsViewController: UIViewController, ViewModelBindableType {
    
    var viewModel: DetailUserSettingsViewModel!
    
    var forecastViewModel: SettingsForecastViewModel!
    
    let disposeBag = DisposeBag()
            
    var tableView: UITableView!
        
    var currentSelectedDay: Day!
        
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupSettings()
        setupUI()
        bindViewModel()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    private func setupSettings() {
        currentSelectedDay = self.viewModel.getForecastSettings()
    }

    private func setupUI() {
        tableView = UITableView(frame: CGRect(x: 0,
                                              y: 0,
                                              width: view.frame.width,
                                              height: view.frame.height),
                                style: .grouped)
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        tableView.isScrollEnabled = false
                
        view.addSubview(tableView)
    }
    
    func bindViewModel() {

        let dataSources = RxTableViewSectionedReloadDataSource<SectionModel<String, Day?>>(configureCell: {
            ds, tv, ip, element in
            let cell = tv.dequeueReusableCell(withIdentifier: "cell")!
            
            if let el = element {
                cell.textLabel?.text = "\(String(describing: el.dayNumber)) days"
            }
            cell.selectionStyle = .none
            
            self.viewModel.dailySettingsList()
                .map { [unowned self] in
                if $0[ip.row] == self.currentSelectedDay {
                    cell.accessoryType = .checkmark
                } else {
                    cell.accessoryType = .none
                }
            }
            .subscribe { _ in }
            .disposed(by: self.disposeBag)
            
            return cell
        }, titleForHeaderInSection: { ds, ip in
            return "Choose time duration"
        })
        
        viewModel.dailySettingsList().map({ el in
            return [SectionModel(model: "", items: el)]
        })
            .observeOn(MainScheduler.instance)
            .bind(to: tableView.rx.items(dataSource: dataSources))
            .disposed(by: disposeBag)
        
        tableView.rx
            .itemSelected
            .flatMap { [unowned self] ip -> Observable<Void> in
                self.viewModel.dailySettingsList().map { [unowned self] item in
                    self.viewModel.update(day: item[ip.row],
                                          forecast: self.currentSelectedDay.selectedDay)
                    self.currentSelectedDay = item[ip.row]
                    self.tableView.reloadData()
            }}
            .asDriver(onErrorJustReturn: ())
            .drive()
            .disposed(by: disposeBag)
    }
}



