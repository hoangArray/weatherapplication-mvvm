//
//  PersistentManager.swift
//  WeatherApplication
//
//  Created by Anton Hoang on 09/12/2019.
//  Copyright © 2019 Anton Hoang. All rights reserved.
//

import Foundation
import CoreData
import RxSwift

class PersistentManager {
    
    let disposeBag = DisposeBag()
    let container: NSPersistentContainer
    
    init() {
        self.container = NSPersistentContainer(name: "WeatherApplication")
    }
    
    func setup() -> Observable<Void> {
        self.container.rx.loadPersistenStore()
            .flatMap { [unowned self] _ -> Observable<Void> in
                return Observable.merge(self.setupDefaultsCities(),
                                        self.setupDefaultSetting())
                
        }
    }

    private func setupDefaultsCities() -> Observable<Void> {

        return Observable<Void>.create { observer -> Disposable in
            let context = self.container.viewContext
            let cityStore = CityStore(managedObjectContext: context)
            let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Cities")
            
            do {
                let res = try context.fetch(request)
                if res.isEmpty {
                    let cityDefault_1 = City(name: "Kharkiv", temp: 0)
                    let cityDefault_2 = City(name: "Kiev", temp: 0)
                    
                    cityStore.insert(cityDefault_1)
                    cityStore.insert(cityDefault_2)
                    observer.onNext(())
                    observer.onCompleted()
                }
            } catch {
                observer.onError(error)
            }
            return Disposables.create()
        }.flatMap { (_) -> Observable<Void> in
            return self.saveContext()
        }
    }
    
    @discardableResult
    private func setupForecastSetting(day: Day) -> Observable<Void> {
        return Observable<Void>.create { obs in
            
            let context = self.container.viewContext
            let request = NSFetchRequest<NSFetchRequestResult>(entityName: "ForecastSettings")
            
            do  {
                let res = try context.fetch(request)
                if res.isEmpty {
                    let forecastDaily = ForecastSettings(context: context)
                    forecastDaily.setValue(false, forKey: "pressure")
                    forecastDaily.setValue(false, forKey: "windSpeed")
                    forecastDaily.setValue(false, forKey: "visibility")
                    
                    forecastDaily.selectedDay = day
                    
                    obs.onNext(())
                    obs.onCompleted()
                }
            } catch {
                obs.onError(error)
            }
            obs.onNext(())
            obs.onCompleted()
            return Disposables.create()
            
        }.flatMap { (_) -> Observable<Void> in
            return self.saveContext()
        }
    }
    
    
    private func setupDefaultSetting() -> Observable<Void> {
        return Observable<Void>.create { (observer) -> Disposable in
            let context = self.container.viewContext
            let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Day")
            do {
                let res = try context.fetch(request)
                if res.isEmpty {
                    let daily_3 = Day(context: context)
                    daily_3.setValue(3, forKey: "dayNumber")
                                                            
                    let daily_7 = Day(context: context)
                    daily_7.setValue(7, forKey: "dayNumber")
                    
                    let daily_11 = Day(context: context)
                    daily_11.setValue(11, forKey: "dayNumber")
                
                    self.setupForecastSetting(day: daily_11)
                        .subscribe { _ in }
                        .disposed(by: self.disposeBag)
    
                    observer.onNext(())
                    observer.onCompleted()
                }
            } catch {
                observer.onError(error)
            }
            observer.onNext(())
            observer.onCompleted()
            return Disposables.create()
        }
        .flatMap { (_) -> Observable<Void> in
            return self.saveContext()
        }
    }
    
    func saveContext() -> Observable<Void> {
        let context = container.viewContext
        if context.hasChanges {
            do {
                try context.save()
                return Observable.just(())
            } catch  {
                return Observable.error(error)
            }
        }
        return Observable.just(())
    }
}

extension Reactive where Base: NSPersistentContainer {
    func loadPersistenStore() -> Observable<Void> {
        return Observable.create { (observer) -> Disposable in
            self.base.loadPersistentStores { (description, error) in
                if let error = error {
                    observer.onError(error)
                    return
                }
                observer.onNext(())
                observer.onCompleted()
            }
            return Disposables.create()
        }
    }
}
