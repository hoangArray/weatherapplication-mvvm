//
//  NSManagedObjectContext+Rx.swift
//  WeatherApplication
//
//  Created by Anton Hoang on 09/12/2019.
//  Copyright © 2019 Anton Hoang. All rights reserved.
//

import Foundation
import CoreData
import RxSwift

protocol ObjectConvertible {
    var identifier: String? { get }
}

protocol ManagedObjectConvertible {
    associatedtype T
    
    @discardableResult
    static func insert(_ object: T, with context: NSManagedObjectContext) -> Observable<T>?
    static func update(_ object: T, with context: NSManagedObjectContext)
    static func delete(_ object: T, with context: NSManagedObjectContext)
    
    @discardableResult
    static func fetchAllObjects(with context: NSManagedObjectContext) -> Observable<[T]>
    static func get(_ object: T, with context: NSManagedObjectContext) -> Self?
            
    func from(_ object: T)
    func toObject() -> T
}

extension ManagedObjectConvertible where T: ObjectConvertible, Self: NSManagedObject {
    
    var identifier: String? {
        return objectID.uriRepresentation().absoluteString
    }
    
    @discardableResult
    static func insert(_ object: T, with context: NSManagedObjectContext) -> Observable<T>? {
        guard object.identifier == nil else { return nil }
        
        let managedObject = Self(context: context)
        managedObject.from(object)
        
        do {
            try context.save()
            return Observable.just(managedObject.toObject())
        } catch {
            return nil
        }
    }
    
    static func update(_ object: T, with context: NSManagedObjectContext) {
        guard let managedObject = get(object, with: context) else { return }
        managedObject.from(object)
        try? context.save()
    }
    
    static func delete(_ object: T, with context: NSManagedObjectContext) {
        guard let managedObject = get(object, with: context) else { return }
        
        context.delete(managedObject)
        print(managedObject)
        try? context.save()
    }
    
    @discardableResult
    static func fetchAllObjects(with context: NSManagedObjectContext) -> Observable<[T]> {
        
        let request = NSFetchRequest<Self>(entityName: String(describing: self))
        request.returnsObjectsAsFaults = false
        
        do {
            let managedObjects = try context.fetch(request)
            
            return Observable.just(managedObjects.map({
                $0.toObject()
            }))
        } catch {
            return Observable.just([])
        }
    }
    static func get(_ object: T, with context: NSManagedObjectContext) -> Self? {
        guard let identifier = object.identifier,
            let url = URL(string: identifier),
        let objectID = context.persistentStoreCoordinator?.managedObjectID(forURIRepresentation: url)
        else { return nil }
        
        do {
            return try context.existingObject(with: objectID) as? Self
        } catch {
            return nil
        }
    }
}

