//
//  ForecastSettingsStore.swift
//  WeatherApplication
//
//  Created by Anton Hoang on 1/18/20.
//  Copyright © 2020 Anton Hoang. All rights reserved.
//

import Foundation
import CoreData
import RxSwift
import RxCocoa

protocol ForecastSettingsStoreProtocol {
    var managedObjectContext: NSManagedObjectContext { get }
    init(managedObjectContext: NSManagedObjectContext)
    func fetchAllForecastSettings() -> Observable<[ForecastSettings]>
    func updateValue(state: Bool, key: String)
    func fetchSwitchStatesObserver() -> Observable<SwitchState>
    func fetchSwitchStates() -> [Bool]
}

class ForecastSettingsStore: ForecastSettingsStoreProtocol {
    
    let disposeBag = DisposeBag()
    var managedObjectContext: NSManagedObjectContext
    
    required init(managedObjectContext: NSManagedObjectContext) {
        self.managedObjectContext = managedObjectContext
    }
    
    func fetchAllForecastSettings() -> Observable<[ForecastSettings]> {
        let request = NSFetchRequest<ForecastSettings>(entityName: "ForecastSettings")
        request.returnsObjectsAsFaults = false
        do {
            let objects = try managedObjectContext.fetch(request)
            return Observable.just(objects.map { $0 })
        } catch { }
         return Observable.just([])
    }
    
    func updateValue(state: Bool, key: String) {
   
        fetchAllForecastSettings()
            .mapMany { [unowned self] in
                let object = $0
                object.setValue(state, forKey: key)
                do {
                    try self.managedObjectContext.save()
                }
                catch {
                    print("\(error.localizedDescription)")
                }}
            .subscribe { _ in }
            .disposed(by: disposeBag)
    }
    
     func fetchSwitchStatesObserver() -> Observable<SwitchState> {
        
        fetchAllForecastSettings().map {
            let last = $0.last
            return SwitchState(pressure: last?.pressure ?? false,
                               windSpeed: last?.windSpeed ?? false,
                               visibility: last?.visibility ?? false)
        }
     }
    
    func fetchSwitchStates() -> [Bool] {
         var states: [Bool] = []
         fetchAllForecastSettings().mapMany {
            states = [$0.pressure, $0.windSpeed, $0.visibility]
         }
         .subscribe { _ in }
         .disposed(by: disposeBag)
         return states
     }
}
