//
//  DailySettingsStore.swift
//  WeatherApplication
//
//  Created by Anton Hoang on 1/1/20.
//  Copyright © 2020 Anton Hoang. All rights reserved.
//

import Foundation
import CoreData
import RxSwift

protocol DailySettingsStorable {
    var managedObjectContext: NSManagedObjectContext { get }
    init(managedObjectContext: NSManagedObjectContext)
    func fetchAllDayObjects() -> Observable<[Day?]>
    func update(day: Day?, forecast: ForecastSettings?)
}

class DailySettingsStore: DailySettingsStorable {
   
    let disposeBag = DisposeBag()
    var managedObjectContext: NSManagedObjectContext
    
    required init(managedObjectContext: NSManagedObjectContext) {
        self.managedObjectContext = managedObjectContext
    }
    
    func fetchAllDayObjects() -> Observable<[Day?]> {
        
        let request = NSFetchRequest<Day>(entityName: String(describing: "Day"))
        request.returnsObjectsAsFaults = false
        
        do {
            let managedObjects = try managedObjectContext.fetch(request)
            return Observable.just(managedObjects.map{ $0 })
        } catch {
            return Observable.just([])
        }
    }
    
    func update(day: Day?, forecast: ForecastSettings?) {
        
        guard let selectedDay = day,
            let forecast = forecast else { return }
        do {
            forecast.selectedDay = selectedDay
            try managedObjectContext.save() 
        } catch { }
    }
}

