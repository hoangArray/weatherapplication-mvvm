//
//  EntityStore.swift
//  WeatherApplication
//
//  Created by Anton Hoang on 14/12/2019.
//  Copyright © 2019 Anton Hoang. All rights reserved.
//

import Foundation
import CoreData
import RxSwift

protocol CityStorable: AnyObject {
    var managedObjectContext: NSManagedObjectContext { get }
    init(managedObjectContext: NSManagedObjectContext)
    func insert(_ object: City)
    func update(_ object: City)
    func delete(_ object: City)
    func fetchAllObjects() -> Observable<[City]>
}

class CityStore: CityStorable {
    
    var managedObjectContext: NSManagedObjectContext
    
    required init(managedObjectContext: NSManagedObjectContext) {
        self.managedObjectContext = managedObjectContext
    }
    
    func insert(_ object: City) {
        Cities.insert(object, with: managedObjectContext)
    }
    
    func update(_ object: City) {
        Cities.update(object, with: managedObjectContext)
    }
    
    func delete(_ object: City) {
        Cities.delete(object, with: managedObjectContext)
    }
    
    func fetchAllObjects() -> Observable<[City]> {
        return Cities.fetchAllObjects(with: managedObjectContext)
    }
}
