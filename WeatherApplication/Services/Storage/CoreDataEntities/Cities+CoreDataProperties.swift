//
//  Cities+CoreDataProperties.swift
//  WeatherApplication
//
//  Created by Anton Hoang on 14/12/2019.
//  Copyright © 2019 Anton Hoang. All rights reserved.
//
//

import Foundation
import CoreData


extension Cities {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Cities> {
        return NSFetchRequest<Cities>(entityName: "Cities")
    }

    @NSManaged public var name: String?
    @NSManaged public var temp: Double
    @NSManaged public var icon: String?
    @NSManaged public var summary: String?

}
