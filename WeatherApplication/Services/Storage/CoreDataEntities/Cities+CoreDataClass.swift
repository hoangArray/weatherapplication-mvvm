//
//  Cities+CoreDataClass.swift
//  WeatherApplication
//
//  Created by Anton Hoang on 14/12/2019.
//  Copyright © 2019 Anton Hoang. All rights reserved.
//
//

import Foundation
import CoreData
import RxSwift

@objc(Cities)
final public class Cities: NSManagedObject {}

extension Cities: ManagedObjectConvertible {
    
    func from(_ object: City) {
        self.name = object.name
        self.temp = object.temp
    }
    
    func toObject() -> City {
        return City(name: name ?? "",
                    temp: temp,
                    identifier: identifier)
    }
}
