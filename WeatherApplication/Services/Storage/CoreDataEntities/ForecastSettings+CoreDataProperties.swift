//
//  ForecastSettings+CoreDataProperties.swift
//  WeatherApplication
//
//  Created by Anton Hoang on 1/18/20.
//  Copyright © 2020 Anton Hoang. All rights reserved.
//
//

import Foundation
import CoreData


extension ForecastSettings {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<ForecastSettings> {
        return NSFetchRequest<ForecastSettings>(entityName: "ForecastSettings")
    }

    @NSManaged public var pressure: Bool
    @NSManaged public var visibility: Bool
    @NSManaged public var windSpeed: Bool
    @NSManaged public var selectedDay: Day?

}
