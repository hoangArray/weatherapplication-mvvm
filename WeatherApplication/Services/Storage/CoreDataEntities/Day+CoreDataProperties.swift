//
//  Day+CoreDataProperties.swift
//  WeatherApplication
//
//  Created by Anton Hoang on 1/12/20.
//  Copyright © 2020 Anton Hoang. All rights reserved.
//
//

import Foundation
import CoreData


extension Day {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Day> {
        return NSFetchRequest<Day>(entityName: "Day")
    }

    @NSManaged public var dayNumber: Int64
    @NSManaged public var selectedDay: ForecastSettings?

}

