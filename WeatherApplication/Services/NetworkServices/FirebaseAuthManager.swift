//
//  FirebaseAuthManager.swift
//  WeatherApplication
//
//  Created by Anton Hoang on 12/18/19.
//  Copyright © 2019 Anton Hoang. All rights reserved.
//

import Foundation
import FirebaseAuth
import RxSwift

protocol FirebaseAuthProviderUseCase {
    typealias CompletionBlock = (_ success: Observable<Bool>) -> ()
    func createUser(email: String, password: String, completionHandler: @escaping CompletionBlock)
    func signIn(email: String, password: String, completionHandler: @escaping CompletionBlock)
}

class FirebaseAuthManager: FirebaseAuthProviderUseCase {
        
    func createUser(email: String, password: String, completionHandler: @escaping CompletionBlock) {
        let res = Auth.auth().rx.createUser(email: email, password: password)
        completionHandler(res)
    }
    
    func signIn(email: String, password: String, completionHandler: @escaping CompletionBlock) {
        let res = Auth.auth().rx.signIn(email: email, password: password)
        completionHandler(res)
    }
}

extension Reactive where Base: Auth {
    
    func createUser(email: String, password: String) -> Observable<Bool> {
        return Observable.create { observer -> Disposable in
            self.base.createUser(withEmail: email, password: password) { (authResult, error) in
                
                guard let _ = authResult?.user else {
                    observer.onError(error!)
                    print("falls")
                    return
                }
                observer.onNext(true)
                observer.onCompleted()
            }
            
            return Disposables.create()
        }
    }
    
    func signIn(email: String, password: String) -> Observable<Bool> {
        return Observable.create { observer -> Disposable in 
            self.base.signIn(withEmail: email, password: password) { (authResult, error) in
                
                if error == nil {
                    print("true")
                    observer.onNext(true)
                    observer.onCompleted()
                } else {
                    guard let error = error else { return }
                    print("crash")
                    observer.onError(error)
                }
            }
            return Disposables.create()
        }
    }
}
