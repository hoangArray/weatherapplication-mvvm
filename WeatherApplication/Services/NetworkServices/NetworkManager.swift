//
//  NetworkManager.swift
//  WeatherApplication
//
//  Created by Anton Hoang on 01/12/2019.
//  Copyright © 2019 Anton Hoang. All rights reserved.
//

import Foundation
import RxSwift
import CoreLocation

enum Services {
    static let API_KEY = "d29608231a1fa722d7e8e50850f324e7"
    static let units = "?units=si"
}

protocol NetworkManagerUseCase {
    typealias CompletionBlock = ((CLLocationCoordinate2D) -> ())
    func updateWeatherForLocation(location: String) -> Observable<Weather>
}

class NetworkManager: NetworkManagerUseCase {
    
    let geocoder = CLGeocoder()
    private func decodeModel<T: Codable>(_ urlString: String? = nil,
                                           request: URLRequest? = nil,
                                         _ model: T.Type) -> Single<T>{
        
        let url = URL(string: urlString ?? "")!
        let request = URLRequest(url: url)
        
        return Single<T>.create{ (single) -> Disposable in
            
            let task = URLSession.shared.dataTask(with: request, completionHandler: { (data, response, error) in
                guard let data = data else { return }
                do {
                    let json: T = try JSONDecoder().decode(model.self, from: data)
                    single(.success(json))
                } catch {
                    single(.error(error))
                }
            })
            task.resume()
            
            return Disposables.create {
                task.cancel()
            }
        }
    }
    
    func findCity() -> Single<Country> {
        let urlString = "https://raw.githubusercontent.com/russ666/all-countries-and-cities-json/6ee538beca8914133259b401ba47a550313e8984/countries.json"
        
        let single: Single<Country> = decodeModel(urlString, Country.self)
        return single
    }
    
    func fetchForecast(location: CLLocationCoordinate2D) -> Single<Weather> {
        let urlString =  "https://api.darksky.net/forecast/\(Services.API_KEY)/\(location.latitude),\(location.longitude)\(Services.units)"
        
        let result: Single<Weather> = decodeModel(urlString, Weather.self)
        return result
    }
    
    func updateWeatherForLocation(location: String) -> Observable<Weather> {
        
        return geocoder.rx.geocodeAddressString(location)
            .filterMap { items in
                guard let first = items.first?.location?.coordinate else {
                    return .ignore
                }
                return .map(first)
            }
            .flatMapLatest { (coordinate: CLLocationCoordinate2D) -> Observable<Weather> in
                self.fetchForecast(location: coordinate).asObservable()
        }
    }
    
    func postForecast<TKey, TValue>(parameters: [TKey: TValue]) -> Single<Country> {
        
        let urlString = ""
        let url = URL(string: urlString)!
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        guard let httpbody = try? JSONSerialization.data(withJSONObject: parameters,
                                                         options: []) else { fatalError() }
        
        request.httpBody = httpbody
        let result: Single<Country> = self.decodeModel(request: request, Country.self)
        return result
    }
}

extension Reactive where Base: CLGeocoder {
    func geocodeAddressString(_ string: String) -> Observable<[CLPlacemark]> {
        return Observable.create { (observer) -> Disposable in
            self.base.geocodeAddressString(string, completionHandler: { (placemark, error) in
                if let error = error {
                    observer.onError(error)
                }
                if let placemark = placemark {
                    observer.onNext(placemark)
                    observer.onCompleted()
                }
            })
            return Disposables.create()
        }
    }
}

extension ObserverType {
}
