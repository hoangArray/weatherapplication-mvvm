//
//  AddCityUseCase.swift
//  WeatherApplication
//
//  Created by Anton Hoang on 30/11/2019.
//  Copyright © 2019 Anton Hoang. All rights reserved.
//

import Foundation
import RxSwift

protocol FindCityProviderUseCase {
    func findCities(_ cityName: String) -> Observable<[String]>
}

class FindCityProviderUseCaseImpl: FindCityProviderUseCase {
    
    let networkManager = NetworkManager()

    func findCities(_ cityName: String) -> Observable<[String]> {
        
        let list: Single<Country> = networkManager.findCity()
        let listRes = list.map { $0.ukraine.filter({ $0.localizedCaseInsensitiveContains(cityName)})}
        return listRes.asObservable()
    }
}
