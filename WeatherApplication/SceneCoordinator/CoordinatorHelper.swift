//
//  CoordinatorHelper.swift
//  WeatherApplication
//
//  Created by Anton Hoang on 12/20/19.
//  Copyright © 2019 Anton Hoang. All rights reserved.
//

import Foundation

protocol CoordinatorHelper {
    func performListOfCities()
}
