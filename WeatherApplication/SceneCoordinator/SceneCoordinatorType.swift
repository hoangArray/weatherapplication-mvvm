//
//  SceneCoordinator.swift
//  WeatherApplication
//
//  Created by Anton Hoang on 30/11/2019.
//  Copyright © 2019 Anton Hoang. All rights reserved.
//

import Foundation
import RxSwift

protocol SceneCoordinatorType {
    @discardableResult
    func transition(to Scene: Scene, with style: TransitionStyle, animated: Bool) -> Completable
    func close(animated: Bool) -> Completable
}
