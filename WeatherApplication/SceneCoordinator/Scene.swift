//
//  Scene.swift
//  WeatherApplication
//
//  Created by Anton Hoang on 30/11/2019.
//  Copyright © 2019 Anton Hoang. All rights reserved.
//

import Foundation
import UIKit

enum Scene {
    case authScreen(AuthViewModel)
    case logInScreen(LoginViewModel)
    case signUpScreen(SignUpViewModel)
    case listOfCities(ListOfCitiesViewModel)
    case findCity(FindCityViewModel)
    case dailyForecast(DailyForecastViewModel)
    case mapScreen(MapViewModel)
    case detailDailyScreen(DetailDailyForecastViewModel)
    case settingsForecastScreen(SettingsForecastViewModel)
    case detailUserSettingsScreen(DetailUserSettingsViewModel)
}

extension Scene {
    
    func instantiate(from storyboard: String = "Main") -> UIViewController {
        let storyboard = UIStoryboard(name: storyboard, bundle: nil)
        
        switch self {
        case .authScreen(let viewModel):
            
            guard let navController = storyboard.instantiateViewController(identifier: "AuthNavController") as? UINavigationController else { fatalError() }
            guard var authVC = navController.viewControllers.first as? AuthViewController else { fatalError() }
            authVC.bind(viewModel: viewModel)
            return navController
            
        case .logInScreen(let viewModel):
            
            guard var logInVC = storyboard.instantiateViewController(withIdentifier: "LoginViewController") as? LoginViewController else { fatalError() }
            logInVC.bind(viewModel: viewModel)
            return logInVC
            
        case .signUpScreen(let viewModel):
            
            guard var signUpVC = storyboard.instantiateViewController(identifier: "SignUpViewController") as? SignUpViewController else { fatalError() }
            signUpVC.bind(viewModel: viewModel)
            return signUpVC
            
        case .listOfCities(let viewModel):
            
            guard var listVC = storyboard.instantiateViewController(identifier: "ListOfCitiesViewController") as? ListOfCitiesViewController else { fatalError() }
            listVC.bind(viewModel: viewModel)
            return listVC
        
            
        case .findCity(let viewModel):

            guard let navVC = storyboard.instantiateViewController(withIdentifier: "addCityNav") as?
                UINavigationController else { fatalError() }
            guard var addCityVC = navVC.viewControllers.first as? FindCityViewController else { fatalError() }
            addCityVC.bind(viewModel: viewModel)
            return addCityVC
            
        case .dailyForecast(let viewModel):
            guard var dailyForecastVC = storyboard.instantiateViewController(withIdentifier: "DailyForecastViewController") as? DailyForecastViewController else { fatalError() }
            dailyForecastVC.bind(viewModel: viewModel)
            return dailyForecastVC
            
        case .mapScreen(let viewModel):
            guard var mapScreen = storyboard.instantiateViewController(identifier: "MapViewController") as? MapViewController else { fatalError() }
            mapScreen.bind(viewModel: viewModel)
            return mapScreen
            
        case .detailDailyScreen(let viewModel):
            guard var detailDailyVC = storyboard.instantiateViewController(identifier: "DetailDailyForecastViewController") as? DetailDailyForecastViewController else { fatalError() }
            detailDailyVC.bind(viewModel: viewModel)
            return detailDailyVC
            
        case .settingsForecastScreen(let viewModel):
            
            guard var settingsVC = storyboard.instantiateViewController(identifier: "SettingsForecastViewController") as? SettingsForecastViewController else { fatalError() }
            settingsVC.bind(viewModel: viewModel)
            return settingsVC
            
        case .detailUserSettingsScreen(let viewModel):
            guard var detailUserSetVC = storyboard.instantiateViewController(identifier: "DetailUserSettingsViewController") as? DetailUserSettingsViewController else { fatalError() }
            detailUserSetVC.bind(viewModel: viewModel)
            return detailUserSetVC
        }
    }
}
