//
//  TransitionModel.swift
//  WeatherApplication
//
//  Created by Anton Hoang on 30/11/2019.
//  Copyright © 2019 Anton Hoang. All rights reserved.
//

import Foundation

enum TransitionStyle {
    case root
    case push
    case modal
}

enum ReturnStyle {
    case pop
}

enum TransitionError: Error {
    case navigationControllerMissing
    case cannotPop
    case uknown
}
