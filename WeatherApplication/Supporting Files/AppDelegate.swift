//
//  AppDelegate.swift
//  WeatherApplication
//
//  Created by Anton Hoang on 24/11/2019.
//  Copyright © 2019 Anton Hoang. All rights reserved.
//

import UIKit
import CoreData
import Swinject
import RxSwift
import Firebase

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    let disposeBag = DisposeBag()
    let persistentManager = PersistentManager()
    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        FirebaseApp.configure()
        let firebaseAuthManager = FirebaseAuthManager()
        
        //storage from data base OR any else services
        let context = persistentManager.container.viewContext
        
        persistentManager.setup()
        .subscribe(onNext: { (_) in
            let networkUseCase = NetworkManager()
            let cityStore = CityStore(managedObjectContext: context)
            let dailySettingsStore = DailySettingsStore(managedObjectContext: context)
            let forecastSettingsStore = ForecastSettingsStore(managedObjectContext: context)
            let findCityUseCase = FindCityProviderUseCaseImpl()
            let coordinator = SceneCoordinator(window: self.window!)
            let commonViewModel = CommonViewModel(sceneCoordinator: coordinator,
                                                  findCityUseCase: findCityUseCase,
                                                  networkUseCase: networkUseCase,
                                                  firebaseAuthManager: firebaseAuthManager,
                                                  cityStore: cityStore,
                                                  dailySettingsStore: dailySettingsStore,
                                                  forecastSettingsStore: forecastSettingsStore)
            
            let authViewModel = AuthViewModel(this: commonViewModel)
            let authScene = Scene.authScreen(authViewModel)
            coordinator.transition(to: authScene, with: .root, animated: false)
        }).disposed(by: disposeBag)

        return true
    }

    func applicationWillTerminate(_ application: UIApplication) {
        persistentManager.saveContext()
    }
}

