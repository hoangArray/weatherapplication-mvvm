//
//  CommonViewModel.swift
//  WeatherApplication
//
//  Created by Anton Hoang on 30/11/2019.
//  Copyright © 2019 Anton Hoang. All rights reserved.
//

import Foundation

class CommonViewModel {
    var sceneCoordinator: SceneCoordinator
    var findCityUseCase: FindCityProviderUseCase
    var networkUseCase: NetworkManagerUseCase
    var firebaseAuthManager: FirebaseAuthProviderUseCase
    var cityStore: CityStorable
    var dailySettingsStore: DailySettingsStorable
    var forecastSettingsStore: ForecastSettingsStoreProtocol
    
    init(sceneCoordinator: SceneCoordinator,
         findCityUseCase: FindCityProviderUseCase,
         networkUseCase: NetworkManagerUseCase,
         firebaseAuthManager: FirebaseAuthProviderUseCase,
         cityStore: CityStorable,
         dailySettingsStore: DailySettingsStorable,
         forecastSettingsStore: ForecastSettingsStoreProtocol
) {
        self.sceneCoordinator = sceneCoordinator
        self.findCityUseCase = findCityUseCase
        self.networkUseCase = networkUseCase
        self.firebaseAuthManager = firebaseAuthManager
        self.cityStore = cityStore
        self.dailySettingsStore = dailySettingsStore
        self.forecastSettingsStore = forecastSettingsStore
    }
}
