//
//  File.swift
//  WeatherApplication
//
//  Created by Anton Hoang on 01/12/2019.
//  Copyright © 2019 Anton Hoang. All rights reserved.
//

import Foundation

struct CityViewModel {
    let name: String
    let temp: Double?
    
    init(city: City) {
        self.name = city.name
        self.temp = city.temp 
    }
}
