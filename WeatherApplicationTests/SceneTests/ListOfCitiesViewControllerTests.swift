//
//  ListOfCitiesViewControllerTests.swift
//  WeatherApplicationTests
//
//  Created by Anton Hoang on 2/1/20.
//  Copyright © 2020 Anton Hoang. All rights reserved.
//

import XCTest
@testable import  WeatherApplication

class ListOfCitiesViewControllerTests: XCTestCase {

    var listOfCitiesViewController: ListOfCitiesViewController!
    
    override func setUp() {
        listOfCitiesViewController = ListOfCitiesViewController()
    }

    override func tearDown() {
        listOfCitiesViewController = nil
    }
    
    func testTableViewIsNotNilWhenLoaded() {}
}
